CC=go build
SOURCES=database_comm.go name_server.go
EXE=NameService
GOFLAGS=-o $(EXE)

all:
	./setup.sh
	$(CC) $(GOFLAGS) $(SOURCES)

clean:
	rm $(EXE)
