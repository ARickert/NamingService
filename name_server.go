package main

import (
	. "fmt"
	"github.com/julienschmidt/httprouter"
	"net/http"
	"encoding/json"
	"os"
	"os/signal"
	"syscall"
	"errors"
)

// Errors
var (
	ServerErrNoConfig = errors.New("Could not configure the server")
)

/* For reference:
type NameData struct {
	Id           int     `json:"id"`
	FirstName    string  `json:"firstName"`
	LastName     string  `json:"lastName"`
	Occurrences  int     `json:"occurrences"`
}
*/

// The data returned from the gender API service
type GenderData struct {
	Name      string  `json:"name"`
	Gender    string  `json:"gender"`
	Samples   int     `json:"samples"`
	Accuracy  int     `json:"accuracy"`
	Duration  string  `json:"duration"`
}

// Data that the server uses to operate. Configured
// through 'serverConfig.json'
type ServerConfiguration struct {
	Port string
	GenderApiKey string
}

// The server that listens from requests from the
// front end
type NameServer struct {
	Router *httprouter.Router
	Db *Database
	Config *ServerConfiguration
}

// An 'Environment' structure which is used to pass
// a database connection to router handlers
type Env struct {
	*NameServer
}

// The 'options' call precedes the 'put/delete' call to do preflight verification
// All we need to do for the options call is verify we want to allow CORS
func (env *Env) PreflightNames(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	// allow cross domain AJAX requests
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")

	// Printf("Hitting the preflight API\n");
}

// GetNames returnes all of the names in the data base or a particular name
// if given the database id (primary key for names)
func (env *Env) GetNames(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {

	// allow cross domain AJAX requests
	w.Header().Set("Access-Control-Allow-Origin", "*")

	// Printf("Hitting the GetNames API\n");
	
	// Get the 'id' value passed in the url
	nameId := ps.ByName("id")

	// There was no 'id' passed so return all the names in the database
	if nameId == "" {
	
		names, err := env.Db.FindAll()
		if err != nil {
			Fprintf(w, "{ \"error\" : \"%s\" }", err)
			return
		}
		
		if namesJson, err := json.Marshal(names); err == nil {
			Fprintf(w, string(namesJson))
		} else {
			Fprintf(w, "{ \"error\" : \"%s\" }", err)
		}
	// Return the data for the specific 'id' passed in ps
	} else {

		name, err := env.Db.FindOneById(nameId);
		if err != nil {
			Fprintf(w, "%s\n", err)
			return
		}
			
		if nameJson, err := json.Marshal(name); err == nil {
			Fprintf(w, string(nameJson))
		} else {
			Fprintf(w, "{ \"error\" : \"%s\" }", err)
		}
	}
}

// doGenderApiCall calls the gender api for a given name
func doGenderApiCall(firstName, genderApiKey string) (*GenderData, error) {

	genderApiAddress := "https://gender-api.com/get?name=" + firstName +
		            "&key=" + genderApiKey

	// Make the call to the gender api
	response, err := http.Get(genderApiAddress)
	if err != nil {
		return nil, err
	} 

	defer response.Body.Close()

	// Store the gender api response in a string to be parsed in a moment 
	var responseData string
	Fscanf(response.Body, "%s", &responseData)

	// Unmarshal gender data string to JSON
	var genderData GenderData
	err = json.Unmarshal([]byte(responseData), &genderData)
	if err != nil {
		return nil, err
	}

	// Fix up the gender string 
	if genderData.Gender == "male" {
		genderData.Gender = "Male"
	} else {
		genderData.Gender = "Female"
	}

	return &genderData, nil
}

// CreateName 
func (env *Env) CreateName(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	// allow cross domain AJAX requests
	w.Header().Set("Access-Control-Allow-Origin", "*")

	// Printf("Hitting the CreateName API\n");

	// Get the name data sent from the front end
	r.ParseForm()
	firstName := r.FormValue("firstName");
	lastName := r.FormValue("lastName");

	genderData, err := doGenderApiCall(firstName, env.Config.GenderApiKey)
	if err != nil {
		Fprintf(w, "{\"error\" : \"%s\"}\n", err)
		return
	}

	// Add the gender data along with the name to the database
	err = env.Db.Create(firstName, lastName, genderData.Gender, genderData.Accuracy)
	if err != nil {
		Fprintf(w, "{\"error\" : \"%s\"}\n", err)
		return
	}

	// Get the database entry corresponding to the first and
	// last name passed to this handler
	name, err := env.Db.FindOneByName(firstName, lastName)
	if err != nil {
		Fprintf(w, "{\"error\" : \"%s\"}\n", err)
		return
	}

	// Turn the database response into a string and return it
	if nameJson, err := json.Marshal(name); err == nil {
		Fprintf(w, string(nameJson))
	} else {
		Fprintf(w, "{ \"error\" : \"%s\" }", err)
		return
	}
}

// UpdateName will increase the occurrence count of the name corresponding
// to the id passed in the url
func (env *Env) UpdateName(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {

	// allow cross domain AJAX requests
	w.Header().Set("Access-Control-Allow-Origin", "*")

	// Printf("Hitting the UpdateName API\n");

	nameId := ps.ByName("id")

	// find the name information corresponding
	name, err := env.Db.FindOneById(nameId)
	if err != nil {
		Fprintf(w, "{ \"error\" : \"Could not get info about the user\" }")	
		return
	}

	// Increase the occurrences for this name in the database
	if err := env.Db.IncrementOccurrences(name.Id); err != nil {
		Fprintf(w, "{ \"error\" : \"%s\" }", err)		
		return
	}

	// Update the data structure after the database update
	name.Occurrences += 1
	
	// Return the updated object
	if nameJson, err := json.Marshal(name); err == nil {
		Fprintf(w, string(nameJson))
	} else {
		Fprintf(w, "{ \"error\" : \"%s\" }", err)
	}
}

// DeleteName will remove a 'name' from the data
func (env *Env) DeleteName(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	// allow cross domain AJAX requests
	w.Header().Set("Access-Control-Allow-Origin", "*")
	
	// Printf("Hitting the Delete API\n");

	nameId := ps.ByName("id")

	// find the name information corresponding
	name, err := env.Db.FindOneById(nameId)
	if err != nil {
		Fprintf(w, "{ \"error\" : \"Could not get info about the user\" }")	
		return
	}

	// Delete this name from the database
	if err := env.Db.Delete(name.Id); err != nil {
		Fprintf(w, "{ \"error\" : \"%s\" }", err)		
		return
	}

	// Return the deleted object
	if nameJson, err := json.Marshal(name); err == nil {
		Fprintf(w, string(nameJson))
	} else {
		Fprintf(w, "{ \"error\" : \"%s\" }", err)
	}
}

// getServerConfiguration will return the data needed to
// service requests
func getServerConfiguration() (*ServerConfiguration, error) {

	file, err := os.Open("serverConfig.json")
	if err != nil {
		return nil, ServerErrNoConfig
	}

	decoder := json.NewDecoder(file)

	serverConfig := ServerConfiguration{}
	err = decoder.Decode(&serverConfig)
	if err != nil {
		return nil, ServerErrNoConfig
	}

	return &serverConfig, nil
}

// NewNameServer allocates the name server to serve requests
func NewNameServer() (*NameServer, error) {

	serverConfig, err := getServerConfiguration()
	if err != nil {
		return nil, err
	}

	router := httprouter.New()

	// Connect to a sqlite database
	if db, err := NewDatabaseConnection("sqlite://name_data.db"); err == nil {
	
		// Server init
		nameServer := &NameServer{router, db, serverConfig}

		// The 'environment' allows us to pass the database
		// connection to request handlers
		env := Env{nameServer}

		// Setup the CRUD routes for names
		router.GET("/names", env.GetNames)
		router.GET("/names/:id", env.GetNames)

		router.POST("/names", env.CreateName)
		router.PUT("/names/:id", env.UpdateName)
		router.DELETE("/names/:id", env.DeleteName)


		// This handles the cross site scripting issue by
		// explicitly handling the 'option' http call that
		// happens before put/delete
		router.OPTIONS("/names", env.PreflightNames)
		router.OPTIONS("/names/:id", env.PreflightNames)

		return nameServer, nil
	} else {
		return nil, err
	}
}

func shutdown(ns *NameServer) {
	Printf("Shutting down the connection and closing the database\n")
	ns.Db.Close()
}

func (ns *NameServer) Serve() {

	// The following makes to possible to run
	signaller := make(chan os.Signal, 1)

	// os.Interrupt is syscall.SIGINT and is garaunteed to
	// be on any platform but syscall.SIGTERM is not
	// garaunteed. If there is a compilated error then
	// remove SIGTERM from this list
	signal.Notify(signaller, os.Interrupt, syscall.SIGTERM)
	go func() {
		// Wait until we receive the interrupt signal then shutdown
		<-signaller
		shutdown(ns)
		os.Exit(0)
	}()
	
	// Serve the mainpage and it's assets from the website directory
	// ns.Router.Handle("/", http.FileServer(http.Dir("./website")))
	ns.Router.ServeFiles("/nameservice/*filepath", http.Dir("./website"))

	// Help message
	Printf("To use the name service open your browser:\n  ==> http://localhost:" + ns.Config.Port + "/nameservice/\n")
	Printf("\nThe current Gender API key is '" + ns.Config.GenderApiKey  + "'. You can change this value in serverConfig.json\n")

	// Begin handling requests
	http.ListenAndServe(":" + ns.Config.Port, ns.Router)
}

func main() {

	// Begin serving requests after configuration
	if nameServer, err := NewNameServer(); err == nil {
		nameServer.Serve()
	} else {
		panic(err)
	}
}
