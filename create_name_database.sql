CREATE TABLE nameinfo
(
        name_id      INTEGER PRIMARY KEY,
        first_name   VARCHAR NOT NULL,
        last_name    VARCHAR NOT NULL,     
        occurrences  INTEGER,
	gender       VARCHAR NOT NULL,
	confidence   INTEGER
);

