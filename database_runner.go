package main

import . "fmt"
// import "errors"
// import "strings"

func main() {

	db, err := NewDatabaseConnection("sqlite://name_data.db")
	if err != nil {
		panic(err)
		return
	}
	defer db.Close()

	Printf("Initial setup\n")

	db.Create("johnny", "dollar")
	db.Create("joe", "dimaggio")
	db.Create("babe", "ruth")
	db.Create("joe", "dimaggio")
	db.Create("crypto", "wizard")
	db.Create("joe", "dimaggio")
	db.Create("babe", "ruth")
	db.Create("time", "man")
	db.PrintAll()

	if name, err := db.FindOne("joe", "dimaggio"); err == nil {

		Printf("Current Occurrences: %d\n", name.occurrences)
		Printf("Deleting an occurrence from Dimaggio\n")
		if err := db.DeleteOccurrence(name.id); err != nil {
			Printf("%s\n", err)
		}

		db.PrintAll()
		Printf("Deleting an occurrence from Dimaggio\n")
		if err := db.DeleteOccurrence(name.id); err != nil {
			Printf("%s\n", err)
		}

		db.PrintAll()
		Printf("Getting rid of Dimaggio\n")
		if err := db.Delete(name.id); err != nil {
			Printf("%s\n", err)
		}		

		db.PrintAll()
	} else {
		Printf("Joe could not be found")
	}

	if name, err := db.FindOne("crypto", "wizard"); err == nil {
		db.PrintAll()
		Printf("Deleting an occurrence from Wizard\n")
		if err := db.DeleteOccurrence(name.id); err != nil {
			Printf("%s\n", err)
		}
		db.PrintAll()

		Printf("Deleting an occurrence from Wizard\n")
		if err := db.DeleteOccurrence(name.id); err != nil {
			Printf("%s\n", err)
		}
		db.PrintAll()
	}
}

