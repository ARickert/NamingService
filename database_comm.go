package main

import (
	_ "github.com/mattn/go-sqlite3"
	. "fmt"
	"database/sql"
	"os"
	"errors"
	"strings"
)

// Database error
var (
	DBErrNameNotFound   = errors.New("The name was not found")
	DBErrNoDialect      = errors.New("The given dialect has not been implemented")
	DBErrNoDialectSep   = errors.New("Dialect seperator '://' not found")
	DBErrNoDatabaseName = errors.New("No database name given")
)

// NameData describes the information about a 'name'
type NameData struct {
	Id           int     `json:"id"`
	FirstName    string  `json:"firstName"`
	LastName     string  `json:"lastName"`
	Occurrences  int     `json:"occurrences"`
	Gender       string  `json:"gender"`
	Confidence   int     `json:"confidence"`
}

// The Database type is an abstraction over different sql dialects
type Database struct {
	*sql.DB
	Dialect string
}

// parseDialectString determines the dialect of sql to use
// currently this only sqlite
func parseDialectString (dialectStr string) ([]string, error) {
	
	var dialectSep int
	if dialectSep = strings.Index(dialectStr, "://"); dialectSep == -1 {
		return nil, DBErrNoDialectSep
	}	
	
	dialect := strings.TrimSpace(dialectStr[:dialectSep])
	databaseName := strings.TrimSpace(dialectStr[dialectSep+3:])

	if len(databaseName) == 0 {
		return nil, DBErrNoDatabaseName
	}
	
	return []string{dialect, databaseName}, nil
}

// Create a database connection for a given sql dialect
func NewDatabaseConnection(dialectStr string) (*Database, error) {

	dialectParameters, err := parseDialectString(dialectStr)
	if err != nil {
		return nil, err
	}

	dialect, databaseName := dialectParameters[0], dialectParameters[1]
	
	if dialect != "sqlite" {
		return nil, DBErrNoDialect
	}

	// Check that the sqlite database file is present 
	_, err = os.Stat(databaseName)
	if err != nil {
		return nil, err
	}

	// Create the connection to the database
	conn, err := sql.Open("sqlite3", "./" + databaseName)
	if err != nil {
		return nil, err
	}

	return &Database{ conn, dialect }, nil
}

// processQueryRowsForName create a NameData value from returned database values
func processQueryRowsForName(rows *sql.Rows, err error) (*NameData, error) {

	if err != nil {
		panic(err)
		return nil, err
	}
	defer rows.Close()

	if rows.Next() {
		var id int
		var firstName string
		var lastName string
		var occurrences int
		var gender string
		var confidence int

		err = rows.Scan(&id, &firstName, &lastName, &occurrences, &gender, &confidence)
		if err != nil {
			return nil, err
		}

		return &NameData{id, firstName, lastName, occurrences, gender, confidence}, nil
	} else {
		return nil, DBErrNameNotFound
	}
}

// FindOneByName will attempt to find a single instance of the name referred to by 'firstName' and 'lastName'
func (dbase *Database) FindOneByName(firstName, lastName string) (*NameData, error) {

	rows, err := dbase.Query("SELECT * FROM nameinfo WHERE first_name == '" + firstName + "' AND last_name == '" + lastName + "'")

	return processQueryRowsForName(rows, err)
}

func (dbase *Database) FindOneById(searchId string) (*NameData, error) {

	rows, err := dbase.Query("SELECT * FROM nameinfo WHERE name_id == " + searchId)

	return processQueryRowsForName(rows, err)
}

// FindAll will return all the name data from the database
func (dbase *Database) FindAll() ([]*NameData, error) {

	rows, err := dbase.Query("SELECT * FROM nameinfo ORDER BY first_name, last_name")

	if err != nil {
		panic(err)
		return nil, err
	}
	defer rows.Close()

	data := make([]*NameData, 0, 100)
	
	var id int
	var firstName string
	var lastName string
	var occurrences int
	var gender string
	var confidence int
	for rows.Next() {
		
		err = rows.Scan(&id, &firstName, &lastName, &occurrences, &gender, &confidence)
		if err != nil {
			return nil, err
		}

		data = append(data, &NameData{id, firstName, lastName, occurrences, gender, confidence})
	}

	return data, nil
}

// Create will add a name to the database
func (dbase *Database) Create(firstName string, lastName string, gender string, confidence int) error {
	
	stmt, err := dbase.Prepare("INSERT INTO nameinfo(first_name, last_name, occurrences, gender, confidence) values(?,?,?,?,?)")
	if err != nil {
		return err
	}

	_, err = stmt.Exec(firstName, lastName, "1", gender, confidence)
	if err != nil {
		return err
	}
	
	return nil
}

// IncrementOccurrences will increment the occurrence count
// for a specific name in the database
func (dbase *Database) IncrementOccurrences(id int) error {

	stmt, err := dbase.Prepare("UPDATE nameinfo SET occurrences = occurrences + 1 WHERE name_id == ?")
	_, err = stmt.Exec(id)
	return err
}

// Delete will remove the name with given 'id' from the database
func (dbase *Database) Delete(id int) error {

	stmt, err := dbase.Prepare("DELETE FROM nameinfo WHERE name_id = ?")
	if err != nil {
		return err
	}

	if _, err = stmt.Exec(id); err != nil {
		return err
	}

	return nil
}

// PrintAll convenience method to print all entries
// in the database
func (dbase *Database) PrintAll() {
	if nds, err := dbase.FindAll(); err == nil {
		
		Printf("  ID                NAME              OCCURRENCES\n")
		Printf("------   --------------------------   -----------\n")
		for _, nd := range nds {
			Printf("%6d   %26s   %11d\n", nd.Id, nd.FirstName + " " + nd.LastName, nd.Occurrences)
		}
		
	} else {
		panic(err)
		return
	}
}
