DATABASE_NAME=name_data.db

############################
# Show the help message
#
if [[ $1 == "--help" ]]; then
    echo "This script will setup the database of names"
    echo ""
    echo "Options include:"
    echo "    --help          Prints this help message"
    echo "    --pre-populate  Populate the newly created database with random name values"
    echo ""
    exit 0
fi

############################
# Create the database and schema
#
if [ ! -e "$DATABASE_NAME" ]; then
    echo "No database named '$DATABASE_NAME' was found so it will be created."
    echo ""
    # Create the named database and exit
    cat create_name_database.sql | sqlite3 "$DATABASE_NAME"
else
    echo "The database named '$DATABASE_NAME' already exists"
    echo ""
fi

############################
# Populate the database with random values
#
if [[ $1 == "--pre-populate" ]]; then
    echo "Populating the database with random name values"
    echo ""
    # TODO
fi
