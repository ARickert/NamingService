********************************
Teaching Strategies Name Service
********************************

I. Building the code

   1) Install the Go language on your machine.

      On a mac for example this means entering the commaand:

          brew install golang

   2) Set your GOPATH

      On a mac or linux this means doing something like the following

          export GOPATH=/home/gadi/Development/GoHome

    3) There are two libraries that need to be installed to build the code.

       To install these libraries enter the following commands

           go get github.com/julienschmidt/httprouter
           go get github.com/mattn/go-sqlite3

    4) Run make

       This is as simple as follows

           make

       The setup.sh script must be executable since it is called by make so,
       you may need to run

           chmod u+x setup.sh

       before running make

II. Running the name service

    1) Run the executable 'NameService'

           ./NameService

       When NameService is run it will tell you how to view the webpage that
       provides the front end for the service

